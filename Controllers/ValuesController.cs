﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Tenat.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly AppDbcontext appContext;

        public ValuesController(AppDbcontext appContext)
        {
            this.appContext = appContext;
        }
        [HttpPost("PostStudent")]
        public async Task<IActionResult> post(Student student)
        {
            await appContext.AddAsync(student);
            await appContext.SaveChangesAsync();
            return Ok();
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> get()
        {
            List<Student> students = await appContext.students.ToListAsync();
            return Ok(students);

        }
    }
}
