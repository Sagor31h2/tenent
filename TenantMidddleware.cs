﻿namespace Tenat
{
    public class TenantMidddleware : IMiddleware
    {
        private readonly IConfiguration configuration;

        public TenantMidddleware(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {


            if(context.Request.Headers.TryGetValue("tenent", out var tenentId))
            {
                Connection.CurrentConnection = configuration.GetConnectionString(tenentId);
            }
            else
            {
                Connection.CurrentConnection = configuration.GetConnectionString("Default");
            }
            await next(context);
        }
    }
}
