﻿using Microsoft.EntityFrameworkCore;

namespace Tenat
{
    public class AppDbcontext : DbContext
    {
        public AppDbcontext(DbContextOptions<AppDbcontext> op) : base(op)
        {

        }
        public DbSet<Student> students { get; set; }
    }
}
